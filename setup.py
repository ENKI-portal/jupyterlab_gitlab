"""
Setup module for the jupyterlab-gitlab proxy extension
"""
import setuptools

setuptools.setup(
    name='jupyterlab_gitlab',
    description='A Jupyter Notebook server extension which acts as a proxy for GitLab API requests.',
    version='0.2.0',
    packages=setuptools.find_packages(),
    author          = 'Mark S. Ghiorso, based on Jupyter Development Team. GitHub',
    author_email    = 'ghiorso@ofm-research.org',
    url             = 'http://enki-portal.org',
    license         = 'BSD',
    platforms       = "Linux, Mac OS X, Windows",
    keywords        = ['Jupyter', 'JupyterLab', 'GitLab'],
    classifiers     = [
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: BSD License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
    ],
    install_requires=[
        'notebook'
    ],
    package_data={'jupyterlab_gitlab':['*']},
)
