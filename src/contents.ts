import {
  Signal, ISignal
} from '@phosphor/signaling';

import {
  PathExt, URLExt
} from '@jupyterlab/coreutils';

import {
  showErrorMessage
} from '@jupyterlab/apputils';


import {
  DocumentRegistry
} from '@jupyterlab/docregistry';

import {
  Contents, ServerConnection,
} from '@jupyterlab/services';

import {
  browserApiRequest, GITLab_API, GitLabRepo,
  GitLabContents, GitLabBlob, GitLabFileContents, GitLabDirectoryListing
} from './gitlab';

import {
  GitLabFileBrowser
} from './browser';

/**
 * A Contents.IDrive implementation that serves as a read-only view onto GitLab repositories.
 */
export
class GitLabDrive implements Contents.IDrive {
  /**
   * Construct a new drive object.
   *
   * @param options - The options used to initialize the object.
   */
  constructor(registry: DocumentRegistry) {
    //console.warn('Entering constructor GitLabDrive');
    //this._serverSettings = ServerConnection.makeSettings();
    this._fileTypeForPath = (path: string) => {
      const types = registry.getFileTypesForPath(path);
      return types.length === 0 ?
             registry.getFileType('text')! :
             types[0];
    };
  }

  /**
   * The name of the drive.
   */
  get name(): 'GitLab' {
    return 'GitLab';
  }

  /**
   * State for whether the user is valid.
   */
  get validUser(): boolean {
    return this._validUser;
  }

  /**
   * Settings for the notebook server.
   */
  readonly serverSettings: ServerConnection.ISettings;

  /**
   * A signal emitted when a file operation takes place.
   */
  get fileChanged(): ISignal<this, Contents.IChangedArgs> {
    return this._fileChanged;
  }

  /**
   * Test whether the manager has been disposed.
   */
  get isDisposed(): boolean {
    return this._isDisposed;
  }

  /**h
   * Dispose of the resources held by the manager.
   */
  dispose(): void {
    if (this.isDisposed) {
      return;
    }
    this._isDisposed = true;
    Signal.clearData(this);
  }

  /**
   * Get the base url of the manager.
   */
  get baseURL(): string {
    return GITLab_API;
  }

  /**
   * Get a file or directory.
   *
   * @param path: The path to the file.
   *
   * @param options: The options used to fetch the file.
   *
   * @returns A promise which resolves with the file content.
   */
  get(path: string, options?: Contents.IFetchOptions): Promise<Contents.IModel> {
    const resource = parsePath(path);
    const group_name = GitLabFileBrowser.group_name;

    // If the user has not been set, return an empty directory placeholder.
    if (resource.user === '') {
      this._validUser = false;
      return Promise.resolve(Private.DummyDirectory);
    }

    // If the user has been set and the repository is empty, list the repositories for the user/group.
    if (resource.user && !resource.repository) {
      return this._listRepos(resource.user);
    }

    // Otherwise identify the repository and get the contents of the appropriate resource.
    let extracode = 'tree'; 
    if (resource.path) {
      const parts = path.split('.');
      if (parts.length > 1) {
        const rpath = URLExt.encodeParts(resource.path).replace('/', '%2F');
        extracode = 'files/' + rpath + '?ref=master';
      } else {
        extracode = 'tree?path=' + URLExt.encodeParts(resource.path);
      }
    }
    let location = resource.user;
    if (group_name !== '') {
      location = group_name;
    }
    const apiPath = URLExt.join(
        URLExt.encodeParts('projects'), 
        URLExt.encodeParts(location)+'%2F'+URLExt.encodeParts(resource.repository), 
        URLExt.encodeParts('repository'),
        extracode);

    return this._apiRequest<GitLabContents>(apiPath).then(contents => {
      this._validUser = true;
      return Private.gitLabContentsToJupyterContents(path, contents, this._fileTypeForPath);
    }).catch((err: ServerConnection.ResponseError) => {
      if (err.response.status === 404) {
        //console.warn('GitLab: cannot find org/repo. Perhaps you misspelled something?');
        this._validUser = false;
        return Private.DummyDirectory;
      } else if (err.response.status === 403 &&
                 err.message.indexOf('rate limit') !== -1) {
        console.error(err.message);
        return Promise.reject(err);
      } else if (err.response.status === 403 &&
                 err.message.indexOf('blob') !== -1) {
        this._validUser = true;
        return this._getBlob(path);
      } else {
        console.error(err.message);
        return Promise.reject(err);
      }
    });
  }

  /**
   * Get an encoded download url given a file path.
   *
   * @param path - An absolute POSIX file path on the server.
   *
   * #### Notes
   * It is expected that the path contains no relative paths,
   * use [[ContentsManager.getAbsolutePath]] to get an absolute
   * path if necessary.
   */
   
  getDownloadUrl(path: string): Promise<string> {
    //console.warn('<><><>Entering getDownloadUrl method of GitLabDrive');
    // Parse the path into user/repository/path
    const resource = parsePath(path);
    if (!resource.user) {
      return Promise.reject('GitLab: no active organization');
    }
    if (!resource.path) {
      return Promise.reject('GitLab: No file selected');
    }
    const dirname = PathExt.dirname(resource.path);
    let location = resource.user;
    if (GitLabFileBrowser.group_name !== '') {
      location = GitLabFileBrowser.group_name;
    }
    const dirApiPath = URLExt.join(
        URLExt.encodeParts('projects'), 
        URLExt.encodeParts(location)+'%2F'+URLExt.encodeParts(resource.repository), 
        URLExt.encodeParts('repository'),
        URLExt.encodeParts('tree')+'?path='+URLExt.encodeParts(dirname));
    //console.warn('...directory path: '+dirApiPath);
    return this._apiRequest<GitLabDirectoryListing>(dirApiPath).then(dirContents => {
      for (let item of dirContents) {
        if (item.path === resource.path) {
          return item.path;
        }
      }
      throw Private.makeError(404, `Cannot find file at ${resource.path}`);
    });
  }

  /**
   * Create a new untitled file or directory in the specified directory path.
   *
   * @param options: The options used to create the file.
   *
   * @returns A promise which resolves with the created file content when the
   *    file is created.
   */
  newUntitled(options: Contents.ICreateOptions = {}): Promise<Contents.IModel> {
    const e = new EvalError('The repository is read-only. Please use the file tab to create a new local file.');
    showErrorMessage('File save error', e);
    return Promise.reject('Repository is read only');
  }

  /**
   * Delete a file.
   *
   * @param path - The path to the file.
   *
   * @returns A promise which resolves when the file is deleted.
   */
  delete(path: string): Promise<void> {
    const e = new EvalError('The repository is read-only. A file in a GitLab repository cannot be deleted from the ENKI server.');
    showErrorMessage('File save error', e);
    return Promise.reject('Repository is read only');
  }

  /**
   * Rename a file or directory.
   *
   * @param path - The original file path.
   *
   * @param newPath - The new file path.
   *
   * @returns A promise which resolves with the new file contents model when
   *   the file is renamed.
   */
  rename(path: string, newPath: string): Promise<Contents.IModel> {
    const e = new EvalError('The repository is read-only. Please use the save-as command to save a copy of this file in a local directory.');
    showErrorMessage('File save error', e);
    return Promise.reject('Repository is read only');
  }

  /**
   * Save a file.
   *
   * @param path - The desired file path.
   *
   * @param options - Optional overrides to the model.
   *
   * @returns A promise which resolves with the file content model when the
   *   file is saved.
   */
  save(path: string, options: Partial<Contents.IModel>): Promise<Contents.IModel> {
    const e = new EvalError('The repository is read-only. Please use the save-as command and specify a local directory.');
    showErrorMessage('File save error', e);
    return Promise.reject('Repository is read only');
  }

  /**
   * Copy a file into a given directory.
   *
   * @param path - The original file path.
   *
   * @param toDir - The destination directory path.
   *
   * @returns A promise which resolves with the new contents model when the
   *  file is copied.
   */
  copy(fromFile: string, toDir: string): Promise<Contents.IModel> {
    const e = new EvalError('The repository is read-only. Please use the save-as command and specify a local directory.');
    showErrorMessage('File save error', e);
    return Promise.reject('Repository is read only');
  }

  /**
   * Create a checkpoint for a file.
   *
   * @param path - The path of the file.
   *
   * @returns A promise which resolves with the new checkpoint model when the
   *   checkpoint is created.
   */
  createCheckpoint(path: string): Promise<Contents.ICheckpointModel> {
    const e = new EvalError('The repository is read-only. A checkpoint cannot be created for this remote access file.');
    showErrorMessage('File save error', e);
    return Promise.reject('Repository is read only');
  }

  /**
   * List available checkpoints for a file.
   *
   * @param path - The path of the file.
   *
   * @returns A promise which resolves with a list of checkpoint models for
   *    the file.
   */
  listCheckpoints(path: string): Promise<Contents.ICheckpointModel[]> {
    return Promise.resolve([]);
  }

  /**
   * Restore a file to a known checkpoint state.
   *
   * @param path - The path of the file.
   *
   * @param checkpointID - The id of the checkpoint to restore.
   *
   * @returns A promise which resolves when the checkpoint is restored.
   */
  restoreCheckpoint(path: string, checkpointID: string): Promise<void> {
    const e = new EvalError('The repository is read-only. Checkpoints cannot be restored for this remote file.');
    showErrorMessage('File save error', e);
    return Promise.reject('Repository is read only');
  }

  /**
   * Delete a checkpoint for a file.
   *
   * @param path - The path of the file.
   *
   * @param checkpointID - The id of the checkpoint to delete.
   *
   * @returns A promise which resolves when the checkpoint is deleted.
   */
  deleteCheckpoint(path: string, checkpointID: string): Promise<void> {
    const e = new EvalError('The repository is read-only. Checkpoints cannot be deleted for this remote file.');
    showErrorMessage('File save error', e);
    return Promise.reject('Read only');
  }

  /**
   * If a file is too large (> 1Mb), we need to access it over the GitLab Git Data API.
   */
  private _getBlob(path: string): Promise<Contents.IModel> {
    //console.warn('Entering _getBlob.');
    let blobData: GitLabFileContents;
    // Get the contents of the parent directory so that we can get the id of the blob.
    const resource = parsePath(path);
    const dirname = PathExt.dirname(resource.path);
    let location = resource.user;
    if (GitLabFileBrowser.group_name !== '') {
      location = GitLabFileBrowser.group_name;
    }    
    const dirApiPath = URLExt.join(
        URLExt.encodeParts('projects'), 
        URLExt.encodeParts(location)+'%2F'+URLExt.encodeParts(resource.repository), 
        URLExt.encodeParts('repository'),
        URLExt.encodeParts('tree')+'?path='+URLExt.encodeParts(dirname));
    //console.warn('... dirApiPath = ' + dirApiPath);
    return this._apiRequest<GitLabDirectoryListing>(dirApiPath).then(dirContents => {
      for (let item of dirContents) {
        if (item.path === resource.path) {
          blobData = item as GitLabFileContents;
          return item.id;
        }
      }
      throw Error('Cannot find id for blob');
    }).then(id => {
      //Once we have the id, form the api url and make the request.
      const blobApiPath = URLExt.join(
        URLExt.encodeParts('projects'), 
        URLExt.encodeParts(location)+'%2F'+URLExt.encodeParts(resource.repository), 
        URLExt.encodeParts('repository'),
        URLExt.encodeParts('files'),
        URLExt.encodeParts(id));
      //console.warn('... blobApiPath = ' + blobApiPath);
      return this._apiRequest<GitLabBlob>(blobApiPath);
    }).then(blob => {
      //Convert the data to a Contents.IModel.
      blobData.content = blob.content;
      //console.warn('... got blob, calling gitLabContentsToJupyterContents');
      return Private.gitLabContentsToJupyterContents(path, blobData, this._fileTypeForPath);
    });
  }

  /**
   * List the repositories for the currently active user.
   * If group is specified, that listing takes precedent over user
   */
  private _listRepos(user: string): Promise<Contents.IModel> {
    //console.warn('Entering _listRepos private method of GitLabDrive');
    return new Promise<Contents.IModel>((resolve, reject) => {
      // Try to find it under orgs.
      let apiPath = URLExt.encodeParts(URLExt.join('users', user, 'projects'));
      if (GitLabFileBrowser.group_name !== '') {
        apiPath = URLExt.encodeParts(URLExt.join('groups', GitLabFileBrowser.group_name, 'projects'));
      }
      this._apiRequest<GitLabRepo[]>(apiPath).then(repos => {
        this._validUser = true;
        resolve(Private.reposToDirectory(repos));
      }).catch((err: ServerConnection.ResponseError) => {
        if (err.response.status === 403 &&
            err.message.indexOf('rate limit') !== -1) {
        } else {
          //console.warn('GitLab: cannot find user. Perhaps you misspelled something?');
          this._validUser = false;
        }
        resolve(Private.DummyDirectory);
      });
    });
  }

  /**
   * Determine whether to make the call via the notebook server proxy or not.
   */
  private _apiRequest<T>(apiPath: string): Promise<T> {
      return browserApiRequest<T>(apiPath);
  }

  private _validUser = false;
  //private _serverSettings: ServerConnection.ISettings;
  private _fileTypeForPath: (path: string) => DocumentRegistry.IFileType;
  private _isDisposed = false;
  private _fileChanged = new Signal<this, Contents.IChangedArgs>(this);
}

/**
 * Specification for a file in a repository.
 */
export
interface GitLabResource {
  readonly user: string;
  readonly repository: string;
  readonly path: string;
}


/**
 * Parse a path into a GitLabResource.
 */
export
function parsePath(path: string): GitLabResource {
  const parts = path.split('/');
  const user = parts.length > 0 ? parts[0] : '';
  const repository = parts.length > 1 ? parts[1] : '';
  const repoPath = parts.length > 2 ? URLExt.join(...parts.slice(2)) : '';
  return { user, repository, path: repoPath };
}

/**
 * Private namespace for utility functions.
 */
namespace Private {
  /**
   * A dummy contents model indicating an invalid or nonexistent repository.
   */
  export
  const DummyDirectory: Contents.IModel = {
    type: 'directory',
    path: '',
    name: '',
    format: 'json',
    content: [],
    created: '',
    writable: false,
    last_modified: '',
    mimetype: '',
  };

  /**
   * Given a JSON GitLabContents object returned by the GitLab API v4,
   * convert it to the Jupyter Contents.IModel.
   *
   * @param path - the path to the contents model in the repository.
   *
   * @param contents - the GitLabContents object.
   *
   * @param fileTypeForPath - a function that, given a path, returns
   *   a DocumentRegistry.IFileType, used by JupyterLab to identify different
   *   openers, icons, etc.
   *
   * @returns a Contents.IModel object.
   */
  export
  function gitLabContentsToJupyterContents(path: string, contents: GitLabContents | GitLabContents[], fileTypeForPath: (path: string) => DocumentRegistry.IFileType): Contents.IModel {
    //console.warn('Entering function gitLabContentsToJupyterContents ');
    if (Array.isArray(contents)) {
      // If we have an array, it is a directory of GitLabContents.
      // Iterate over that and convert all of the items in the array/
      //console.warn('... an array.');
      return {
        name: PathExt.basename(path),
        path: path,
        format: 'json',
        type: 'directory',
        writable: false,
        created: '',
        last_modified: '',
        mimetype: '',
        content: contents.map( c => {
          return gitLabContentsToJupyterContents(
            PathExt.join(path, c.name), c, fileTypeForPath);
        })
      } as Contents.IModel;
    } else if (contents.type === 'file' || contents.type === 'symlink'  
            || contents.type === 'blob' || !contents.type) {
      //console.warn('... a file or blob.');
      // If it is a file or blob, convert to a file
      const fileType = fileTypeForPath(path);
      const fileContents = (contents as GitLabFileContents).content;
      let content: any;
      switch (fileType.fileFormat) {
        case 'text':
          content = fileContents !== undefined ? atob(fileContents) : null;
          break;
        case 'base64':
          content = fileContents !== undefined ? fileContents : null;
          break;
        case 'json':
          content = fileContents !== undefined ? JSON.parse(atob(fileContents)) : null;
          break;
        default:
          throw new Error(`Unexpected file format: ${fileType.fileFormat}`);
      }
      return {
        name: PathExt.basename(path),
        path: path,
        format: fileType.fileFormat,
        type: 'file',
        created: '',
        writable: false,
        last_modified: '',
        mimetype: fileType.mimeTypes[0],
        content
      };
    } else if (contents.type === 'dir' || contents.type === 'tree') {
      //console.warn('... a directory.');
      // If it is a directory, convert to that.
      return {
        name: PathExt.basename(path),
        path: path,
        format: 'json',
        type: 'directory',
        created: '',
        writable: false,
        last_modified: '',
        mimetype: '',
        content: null
      };
    } else if (contents.type === 'submodule') {
      // If it is a submodule, throw an error.
      throw makeError(400, `Cannot open "${contents.name}" because it is a submodule`);
    } else {
      throw makeError(500, `"${contents.name}" has and unexpected type: ${contents.type}`);
    }
  }

  /**
   * Given an array of JSON GitLabRepo objects returned by the GitLab API v4,
   * convert it to the Jupyter Contents.IModel conforming to a directory of
   * those repositories.
   *
   * @param repo - the GitLabRepo object.
   *
   * @returns a Contents.IModel object.
   */
  export
  function reposToDirectory(repos: GitLabRepo[]): Contents.IModel {
    //console.warn('Entering function reposToDirectory');
    // If it is a directory, convert to that.
    let content: Contents.IModel[] = repos.map( repo => {
      //console.warn('... repo.name = ' + repo.name);
      return {
        name: repo.name,
        path: repo.name,
        format: 'json',
        type: 'directory',
        created: '',
        writable: false,
        last_modified: '',
        mimetype: '',
        content: null
      } as Contents.IModel;
    });

    return {
      name: '',
      path: '',
      format: 'json',
      type: 'directory',
      created: '',
      last_modified: '',
      writable: false,
      mimetype: '',
      content
    };
  }

  /**
   * Wrap an API error in a hacked-together error object
   * masquerading as an `ServerConnection.ResponseError`.
   */
  export
  function makeError(code: number, message: string): ServerConnection.ResponseError {
    const response = new Response(message, { status: code, statusText: message });
    return new ServerConnection.ResponseError(response, message);
  }
}
