import {
  PanelLayout, Widget
} from '@phosphor/widgets';

import {
  ToolbarButton
} from '@jupyterlab/apputils';

import {
  URLExt
} from '@jupyterlab/coreutils';

import {
  ObservableValue
} from '@jupyterlab/observables';

import {
  FileBrowser
} from '@jupyterlab/filebrowser';

import {
  GitLabDrive, parsePath
} from './contents';

/**
 * The GitLab base url.
 */
const GITLab_BASE_URL = 'https://gitLab.com';

/**
 * Widget for hosting the GitLab filebrowser.
 */
export
class GitLabFileBrowser extends Widget {
  static user_name: string;
  static private_key: string;
  static group_name: string;

  constructor(browser: FileBrowser, drive: GitLabDrive, initUser: string, initCode: string, initGroup: string) {
    super();
    //console.warn('Entering constructor for GitLabFileBrowser');
    this.addClass('jp-GitLabBrowser');
    this.layout = new PanelLayout();
    (this.layout as PanelLayout).addWidget(browser);
    this._browser = browser;
    this._drive = drive;

    GitLabFileBrowser.user_name = initUser;
    GitLabFileBrowser.private_key = initCode;
    GitLabFileBrowser.group_name = initGroup;

    // Create an editable name for the user name.
    this.userName = new GitLabEditableName('', '<Edit User>');
    this.userName.addClass('jp-GitLabEditableUserName');
    this.userName.node.title = 'Click to edit user';
    //this._browser.toolbar.addItem('user', this.userName);
    this.userName.name.changed.connect(this._onUserChanged, this);

    // Create an editable name for the user/org access code.
    this.userCode = new GitLabEditableName('', '<Priv Code>');
    this.userCode.addClass('jp-GitLabEditableUserCode');
    this.userCode.node.title = 'Click to edit access code';
    //this._browser.toolbar.addItem('code', this.userCode);
    this.userCode.name.changed.connect(this._onCodeChanged, this);

    // Create an editable name for the group name.
    this.groupName = new GitLabEditableName('', '<Edit Group>');
    this.groupName.addClass('jp-GitLabEditableGroupName');
    this.groupName.node.title = 'Click to edit group';
    //this._browser.toolbar.addItem('group', this.groupName);
    this.groupName.name.changed.connect(this._onGroupChanged, this);

    // Create a button that opens GitLab at the appropriate repo+directory.
    this._openGitLabButton = new ToolbarButton({
      onClick: () => {
        let url = GITLab_BASE_URL;
        // If there is no valid user, open the GitLab homepage.
        if (!this._drive.validUser) {
          window.open(url);
          return;
        }
        //console.warn('... open: ' + this._browser.model.path.split(':')[1]);
        const resource = parsePath(this._browser.model.path.split(':')[1]);
        url = URLExt.join(url, resource.user);
        if (resource.repository) {
          url = URLExt.join(url, resource.repository,
                            'tree', 'master', resource.path);
        }
        window.open(url);
      },
      className: 'jp-GitLabIcon',
      tooltip: 'Open this repository on GitLab'
    });
    this._browser.toolbar.addItem('GitLab', this._openGitLabButton);

  }

  /**
   * An editable widget hosting the current user name.
   */
  readonly userName: GitLabEditableName;

  /**
   * An editable widget hosting the current user private acceess code.
   */
  readonly userCode: GitLabEditableName;

  /**
   * An editable widget hosting the current group name.
   */
  readonly groupName: GitLabEditableName;

  /**
   * React to a change in user.
   */
  private _onUserChanged(sender: ObservableValue, args: ObservableValue.IChangedArgs) {
    GitLabFileBrowser.user_name = String(sender.get());
    if (this._changeGuard) {
      return;
    }
    this._changeGuard = true;
    //console.warn("On user changed called.");
    this._browser.model.cd(`/${args.newValue as string}`).then(() => {
      this._changeGuard = false;
      this._updateErrorPanel();
      // Once we have the new listing, maybe give the file listing
      // focus. Once the input element is removed, the active element
      // appears to revert to document.body. If the user has subsequently
      // focused another element, don't focus the browser listing.
      if (document.activeElement === document.body) {
        const listing = (this._browser.layout as PanelLayout).widgets[2];
        listing.node.focus();
      }
    });
  }

  /**
   * React to a change in code.
   */
  private _onCodeChanged(sender: ObservableValue, args: ObservableValue.IChangedArgs) {
    //console.warn('_onCodeChanged called = ' + sender.get());
    GitLabFileBrowser.private_key = String(sender.get());
  }

  /**
   * React to a change in group.
   */
  private _onGroupChanged(sender: ObservableValue, args: ObservableValue.IChangedArgs) {
    GitLabFileBrowser.group_name = String(sender.get());
  }

  /**
   * React to a change in the validity of the drive.
   */
  private _updateErrorPanel(): void {
    const resource = parsePath(this._browser.model.path.split(':')[1]);
    const validUser = this._drive.validUser;

    // If we currently have an error panel, remove it.
    if (this._errorPanel) {
      const listing = (this._browser.layout as PanelLayout).widgets[2];
      listing.node.removeChild(this._errorPanel.node);
      this._errorPanel.dispose();
      this._errorPanel = null;
    }

    // If we have an invalid user, make an error panel.
    if (!validUser) {
      const message = resource.user ?
        `"${resource.user}" appears to be an invalid user name!` :
        'Please enter a GitLab user name';
      this._errorPanel = new GitLabErrorPanel(message);
      const listing = (this._browser.layout as PanelLayout).widgets[2];
      listing.node.appendChild(this._errorPanel.node);
      return;
    }
  }

  private _browser: FileBrowser;
  private _drive: GitLabDrive;
  private _errorPanel: GitLabErrorPanel | null;
  private _openGitLabButton: ToolbarButton;
  private _changeGuard = false;
}

/**
 * A widget that hosts an editable field, which holds the currently active GitLab user name.
 */
export
class GitLabEditableName extends Widget {
  constructor(initialName: string = '', placeholder?: string) {
    super();
    this.addClass('jp-GitLabEditableName');
    this._nameNode = document.createElement('div');
    this._nameNode.className = 'jp-GitLabEditableName-display';
    this._editNode = document.createElement('input');
    this._editNode.className = 'jp-GitLabEditableName-input';

    this._placeholder = placeholder || '<Edit Name>';

    this.node.appendChild(this._nameNode);
    this.name = new ObservableValue(initialName);
    this._nameNode.textContent = initialName || this._placeholder;

    this.node.onclick = () => {
      if (this._pending) {
        return;
      }
      this._pending = true;
      Private.changeField(this._nameNode, this._editNode).then(value => {
        this._pending = false;
        if (this.name.get() !== value) {
          this.name.set(value);
        }
      });
    };

    this.name.changed.connect((s, args) => {
      if (args.oldValue !== args.newValue) {
        this._nameNode.textContent =
          args.newValue as string || this._placeholder;
      }
    });
  }

  /**
   * The current name of the field.
   */
  readonly name: ObservableValue;


  private _pending  = false;
  private _placeholder: string;
  private _nameNode: HTMLElement;
  private _editNode: HTMLInputElement;
}

/**
 * A widget hosting an error panel for the browser,
 * used if there is an invalid user name or if we
 * are being rate-limited.
 */
export
class GitLabErrorPanel extends Widget {
  constructor(message: string) {
    super();
    this.addClass('jp-GitLabErrorPanel');
    const image = document.createElement('div');
    const text = document.createElement('div');
    image.className = 'jp-GitLabErrorImage';
    text.className = 'jp-GitLabErrorText';
    text.textContent = message;
    this.node.appendChild(image);
    this.node.appendChild(text);
  }
}


/**
 * A module-Private namespace.
 */
namespace Private {
  export
  /**
   * Given a text node and an input element, replace the text
   * node wiht the input element, allowing the user to reset the
   * value of the text node.
   *
   * @param text - The node to make editable.
   *
   * @param edit - The input element to replace it with.
   *
   * @returns a Promise that resolves when the editing is complete,
   *   or has been canceled.
   */
  function changeField(text: HTMLElement, edit: HTMLInputElement): Promise<string> {
    // Replace the text node with an the input element.
    let parent = text.parentElement as HTMLElement;
    let initialValue = text.textContent || '';
    edit.value = initialValue;
    parent.replaceChild(edit, text);
    edit.focus();

    // Highlight the input element
    let index = edit.value.lastIndexOf('.');
    if (index === -1) {
      edit.setSelectionRange(0, edit.value.length);
    } else {
      edit.setSelectionRange(0, index);
    }

    return new Promise<string>((resolve, reject) => {
      edit.onblur = () => {
        // Set the text content of the original node, then
        // replace the node.
        parent.replaceChild(text, edit);
        text.textContent = edit.value || initialValue;
        resolve(edit.value);
      };
      edit.onkeydown = (event: KeyboardEvent) => {
        switch (event.keyCode) {
        case 13:  // Enter
          event.stopPropagation();
          event.preventDefault();
          edit.blur();
          break;
        case 27:  // Escape
          event.stopPropagation();
          event.preventDefault();
          edit.value = initialValue;
          edit.blur();
          break;
        default:
          break;
        }
      };
    });
  }
}
